sampler_method = "mcmc"

[nested]
nlive = 500
bound = "multi"
sample = "auto"

[kern]
uv_bins_du = 20
fg = [ "fg_int", "fg_mix",]
eor = [ "eor_vae",]

[mcmc]
n_steps = 400
n_walkers = 200
n_burn = 300
move = "kde"

[fg_int]
type = "MRBF"

[fg_mix]
type = "MMat52"

[eor_exp]
type = "MExponential"

[ultranest]
nlive = 200

[fg_no]
type = "MRBF"

[fg_excess]
type = "MMat52"

[eor_vae]
type = "VAEKernTorch"
fitter_filename = "VAE_models/vae_21cmFAST_3params_2_z10.1_n5000_med_beta.pt"

[eor_iae_n50]
type = "IAEKern"
fitter_filename = "IAE_models/080921_iae_21cmFAST_3params_v2_AP_2_z10.1_n50.pkl"

[eor_iae_n500]
type = "IAEKern"
fitter_filename = "IAE_models/080921_iae_21cmFAST_3params_v2_AP_2_z10.1_n500.pkl"

[eor_iae_n5000]
type = "IAEKern"
fitter_filename = "IAE_models/080921_iae_21cmFAST_3params_v2_AP_2_z10.1_n5000.pkl"

[eor_m32]
type = "MMat32"

[kern.noise]
estimate_baseline_noise_from_stokes = "V"
estimate_baseline_noise_remove_n_pca = 0

[fg_int.variance]
prior = "Log10Uniform(-0.1, 0.2)"
log_scale = true

[fg_int.lengthscale]
prior = "Uniform(40, 55)"

[fg_int.ls_alpha]
prior = "Fixed(0)"

[fg_int.var_alpha]
prior = "Fixed(0)"

[fg_mix.variance]
prior = "Log10Uniform(-3, -2)"
log_scale = true

[fg_mix.lengthscale]
prior = "Uniform(2, 6)"

[fg_mix.ls_alpha]
prior = "Uniform(0.5, 1.5)"

[fg_mix.var_alpha]
prior = "Fixed(0)"

[eor_exp.lengthscale]
prior = "Uniform(0.2, 1.5)"

[eor_exp.variance]
prior = "Log10Uniform(-7, -4)"
log_scale = true

[eor_exp.ls_alpha]
prior = "Fixed(0)"

[eor_exp.var_alpha]
prior = "Fixed(0)"

[fg_no.variance]
prior = "Fixed(1e-20)"

[fg_no.lengthscale]
prior = "Fixed(10)"

[fg_no.ls_alpha]
prior = "Fixed(0)"

[fg_no.var_alpha]
prior = "Fixed(0)"

[fg_excess.variance]
prior = "Log10Uniform(-6, -4)"
log_scale = true

[fg_excess.lengthscale]
prior = "Uniform(0.1, 0.8)"

[fg_excess.ls_alpha]
prior = "Fixed(0)"

[fg_excess.var_alpha]
prior = "Fixed(0)"

[eor_vae.x1]
prior = "Uniform(-3, 3)"

[eor_vae.x2]
prior = "Uniform(-3, 3)"

[eor_vae.variance]
prior = "Log10Uniform(-6, -4)"
log_scale = true

[eor_iae_n50.x1]
prior = "Uniform(-1.3647482, 1.2834153)"

[eor_iae_n50.x2]
prior = "Uniform(-0.8, 1.2)"

[eor_iae_n50.variance]
prior = "Log10Uniform(-7, -4)"
log_scale = true

[eor_iae_n500.x1]
prior = "Uniform(-0.8292264, 1.25)"

[eor_iae_n500.x2]
prior = "Uniform(-0.8, 1.2)"

[eor_iae_n500.variance]
prior = "Log10Uniform(-7, -4)"
log_scale = true

[eor_iae_n5000.x1]
prior = "Uniform(-0.8292264, 1.25)"

[eor_iae_n5000.x2]
prior = "Uniform(-0.8, 1.2)"

[eor_iae_n5000.variance]
prior = "Log10Uniform(-7, -4)"
log_scale = true

[eor_m32.lengthscale]
prior = "Uniform(0.2, 1.5)"

[eor_m32.variance]
prior = "Log10Uniform(-7, -4)"
log_scale = true

[eor_m32.ls_alpha]
prior = "Fixed(0)"

[eor_m32.var_alpha]
prior = "Fixed(0)"

[kern.noise.alpha]
prior = "Fixed(1)"
