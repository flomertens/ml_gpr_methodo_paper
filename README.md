# Retrieving the 21-cm signal from the Epoch of Reionization with learnt Gaussian process kernels

This repository provide all the code, data and trained models to reproduce the results from the paper [Retrieving the 21-cm signal from the Epoch of Reionization with learnt Gaussian process kernels](https://arxiv.org/abs/2307.13545).

The VAE, simulated cube generation and power-spectra generation code is available in the package [ps-eor](https://pypi.org/project/ps-eor/). The IAE code is available in the code/ directory.


